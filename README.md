<p align="center">
   <img src="https://images.gitee.com/uploads/images/2021/0523/100003_b205be64_478496.png" width="200" height="200" />
</p>

<h1 align="center"> 为梦想而创作：RXThinkCMF_AVL8_PRO权限(RBAC)及内容管理框架</h1>

## 项目介绍
是一款基于Laravel8.x+Vue+或AntDesign实现的前后端分离架构一站式系统平台开发框架，拥有完善的(RBAC)权限架构：用户、角色、菜单、独立权限、角色权限等等,系统框架权限颗粒度精细化控制到按钮节点级别，前端采用全新的UI框架，支持多主题切换，兼容手机终端、PAD终端和PC电脑终端，用户使用体验非常好，为了避免重复造轮子，系统在设计之初便规划并集成了常规管理模块，包括：完整的权限架构管理分配模块、字典管理、配置管理、行政区划管理、广告管理、页面推荐管理、系统日志、网站配置管理等等；为了提高开发效率和降低人力成本，缩短研发周期，系统框架集成了代码生成器，内置平台自定义研发的模板引擎，可以根据表结构一键生成CRUD整个模块的全部文件和业务实现代码，同时自动化生成菜单和权限节点，赋予权限后即可进行使用，同时也加入如统计报表等等相关统计模块，开发者可以基于框架做二次开发，可以用户个人项目、公司项目以及客户定制化项目，本框架为一站式系统框架开发平台，可以帮助开发者提升开发效率、降低研发成本，同时便于后期的系统维护升级，欢迎大家使用，我们将全力给您提供技术支持！！ 

## 环境要求:
* PHP >= 7.3及以上
* PDO PHP Extension
* MBstring PHP Extension
* CURL PHP Extension
* 开启静态重写
* 要求环境支持pathinfo
* 要求安装Zip扩展(插件/模块市场需要)

### 功能特性
- **严谨规范：** 提供一套有利于团队协作的结构设计、编码、数据等规范。
- **高效灵活：** 清晰的分层设计、钩子行为扩展机制，解耦设计更能灵活应对需求变更。
- **严谨安全：** 清晰的系统执行流程，严谨的异常检测和安全机制，详细的日志统计，为系统保驾护航。
- **组件化：** 完善的组件化设计，丰富的表单组件，让开发列表和表单更得心应手。无需前端开发，省时省力。
- **简单上手快：** 结构清晰、代码规范、在开发快速的同时还兼顾性能的极致追求。
- **自身特色：** 权限管理、组件丰富、第三方应用多、分层解耦化设计和先进的设计思想。
- **高级进阶：** 分布式、负载均衡、集群、Redis、分库分表。 
- **命令行：** 命令行功能，一键管理应用扩展。 


## 开发者信息
* 系统名称：RXThinkCMF_AVL8_PRO前后端分离旗舰版
* 作者：三只松鼠
* 作者QQ：1175401194  
* 官网网址：[http://www.rxthink.cn/](http://www.rxthink.cn/)  
* 文档网址：[http://docs.avl.pro.rxthink.cn/](http://docs.avl.pro.rxthink.cn/)  
* 开源协议：Apache 2.0

### RXThinkCMF版本说明

| 版本名称 | 说明 | 地址 |
| :---: | :---: | :---: |
| RXThinkCMF_TP3.2专业版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP3.2 |
| RXThinkCMF_TP3.2旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP3.2_PRO |
| RXThinkCMF_TP5.1专业版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP5.1 |
| RXThinkCMF_TP5.1旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP5.1_PRO |
| RXThinkCMF_TP6.x专业版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP6 |
| RXThinkCMF_TP6.x旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP6_PRO |
| RXThinkCMF_LV5.8专业版 | 最新开源版本，master分支 | https://gitee.com/laravel520/RXThinkCMF_LV5.8 |
| RXThinkCMF_LV5.8旗舰版 | 最新开源版本，master分支 | https://gitee.com/laravel520/RXThinkCMF_LV5.8_PRO |
| ThinkPhp3.2+Vue+ElementUI旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP3.2_PRO |
| ThinkPhp3.2+Vue+AntDesign旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_AVTP3.2_PRO |
| ThinkPhp5.1+Vue+ElementUI旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP5.1_PRO |
| ThinkPhp5.1+Vue+AntDesign旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_AVTP5.1_PRO |
| ThinkPhp6.x+Vue+ElementUI旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP6_PRO |
| ThinkPhp6.x+Vue+AntDesign旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_AVTP6_PRO |
| Laravel8.x+Vue+ElementUI旗舰版 | 最新开源版本，master分支 | https://gitee.com/laravel520/RXThinkCMF_EVL8_PRO |
| Laravel8.x+Vue+AntDesign旗舰版 | 最新开源版本，master分支 | https://gitee.com/laravel520/RXThinkCMF_AVL8_PRO |

## 后台演示

- 演示地址：[http://manage.avl.pro.rxthink.cn/](http://manage.avl.pro.rxthink.cn/)
- 演示账号：admin
- 演示密码：123456

## 技术支持

[技术支持QQ：1175401194](http://wpa.qq.com/msgrd?v=3&amp;uin=1175401194&amp;site=qq&amp;menu=yes)

## 效果图展示

#### 效果图1
 ![效果图1](./public/uploads/demo/1.png)

#### 效果图2
 ![效果图2](./public/uploads/demo/2.png)
 
#### 效果图3
 ![效果图3](./public/uploads/demo/3.png)
 
#### 效果图4
 ![效果图4](./public/uploads/demo/4.png)
 
#### 效果图5
 ![效果图5](./public/uploads/demo/5.png)
 
#### 效果图6
 ![效果图5](./public/uploads/demo/6.png)
 
#### 效果图7
 ![效果图7](./public/uploads/demo/7.png)
 
#### 效果图8
 ![效果图8](./public/uploads/demo/8.png)
 
#### 效果图9
 ![效果图9](./public/uploads/demo/9.png)
 
#### 效果图10
 ![效果图10](./public/uploads/demo/10.png)
 
#### 效果图11
 ![效果图11](./public/uploads/demo/11.png)
 
#### 效果图12
 ![效果图12](./public/uploads/demo/12.png)
 
#### 效果图13
![效果图13](./public/uploads/demo/13.png)

#### 效果图14
![效果图14](./public/uploads/demo/14.png)

#### 效果图15
![效果图15](./public/uploads/demo/15.png)

#### 效果图16
![效果图16](./public/uploads/demo/16.png)

#### 效果图17
![效果图17](./public/uploads/demo/17.png)

#### 效果图18
![效果图18](./public/uploads/demo/18.png)

#### 效果图19
![效果图19](./public/uploads/demo/19.png)

#### 效果图20
![效果图20](./public/uploads/demo/20.png)

## 安全&缺陷
如果你发现了一个安全漏洞或缺陷，请发送邮件到 1175401194@qq.com,所有的安全漏洞都将及时得到解决。

## 鸣谢
感谢[AntDesign](https://2x.antdv.com/components/transfer-cn)、[Vue](https://2x.antdv.com/components/transfer-cn)等优秀开源项目。

## 版权信息

提供个人非商业用途免费使用，商业需授权。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2017~2020 rxthink.cn (http://www.rxthink.cn)

All rights reserved。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
