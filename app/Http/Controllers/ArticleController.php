<?php


namespace App\Http\Controllers;


use App\Services\ArticleService;

/**
 * 文章管理-控制器
 * @author 牧羊人
 * @since 2021/6/9
 * Class ArticleController
 * @package App\Http\Controllers
 */
class ArticleController extends Backend
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2021/6/9
     * ArticleController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new ArticleService();
    }
}
